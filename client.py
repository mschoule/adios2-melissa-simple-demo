"""Adios2/Melissa dummy data generator"""
import adios2
import argparse
import numpy as np
import os
import time

from datetime import datetime
from mpi4py import MPI
from server import compute_load


def melissa_generate_data(n_step: int = 1):
    """
    This function generates and sends data with adios2
    """

    # MPI initialization
    comm = MPI.COMM_WORLD
    comm_size = comm.Get_size()
    rank = comm.Get_rank()

    print(f"Start simulation on proc {rank}..")

    # Set up data production
    sim_id = os.getenv("SIMU_ID")
    file_name = f"melissa.sid-{sim_id}"
    field = "array"
    vect_size = 10
    i_1, i_n = compute_load(rank, vect_size, comm_size)
    local_vect_size = i_n - i_1 + 1
    print(f"Rank {rank}, local_vect_size {local_vect_size}, i_1 {i_1}, i_n {i_n}")
    if sim_id:
        vect = np.array([1. * i for i in range(i_1, i_n + 1)]) * (int(sim_id) + 1)

    # ADIOS2
    adios = adios2.ADIOS(comm=comm)
    io = adios.DeclareIO("writerIO")
    io.SetEngine("SST")
    print(f"Setting distribution mode to {args.step_mode}")
    io.SetParameters({"StepDistributionMode": args.step_mode})  # , "SstVerbose": "4"})
    # io.SetParameter("StepDistributionMode", args.step_mode)
    shape = [vect_size]  # global size
    count = [local_vect_size]  # local size
    start = [i_1]  # start of local vector wrt the global one
    vect_id = io.DefineVariable(field, vect, shape, start, count, adios2.ConstantDims)
    print(f"Set engine at {datetime.now()}, filename {file_name}")
    engine = io.Open(file_name, adios2.Mode.Write)

    # Time loop
    for it in range(n_step):
        engine.BeginStep()
        engine.Put(vect_id, vect)
        engine.EndStep()
        print(f"Time step {it} sends {vect} (me={rank})")

    engine.Close()
    print("Finish simulation.")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--n_step", type=int, default=1, help="number of time-steps to produce")
    parser.add_argument("--step_mode", type=str, default="AllToAll", help="step distribution mode")
    args = parser.parse_args()

    t0 = time.time()
    melissa_generate_data(args.n_step)

    tf = time.time()
    print(f"Elapsed time: {tf-t0} seconds")
