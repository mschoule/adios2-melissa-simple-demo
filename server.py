"""Adios2/Melissa dummy data consumer"""
import adios2
import argparse
import numpy as np
import numpy.typing as npt
import threading
import time

from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from mpi4py import MPI
from typing import Dict, Tuple, List, Optional
import os
import json


def compute_load(rank: int, vect_size: int, comm_size: int) -> Tuple[int, int]:
    """
    this function computes the load per server rank
    """
    card: int = vect_size // comm_size
    excess: int = vect_size % comm_size

    if rank < excess:
        i_1 = rank * card + rank + 1
        i_n = i_1 + card
    else:
        i_1 = rank * card + excess + 1
        i_n = i_1 + card - 1
    return i_1 - 1, i_n - 1


def circular_integer_generator(numbers):
    """
    Generates integers from the given list in a circular manner.

    Parameters:
        numbers (list): A list of integers.

    Yields:
        int: The next integer in the circular sequence.
    """
    if not numbers or not all(isinstance(num, int) for num in numbers):
        raise ValueError("Input must be a non-empty list of integers.")

    index = 0
    length = len(numbers)
    print(f"length of generator {length}")

    while True:
        yield numbers[index]
        index = (index + 1) % length


class Server:
    def __init__(
            self, n_client: int, client_np: int, n_step: int, thread_open: bool, thread_data: bool, step_mode: str = "AllToAll", n_server: int = 2
    ):
        # Study parameters
        self.n_client = n_client
        self.client_np = client_np
        self.n_step = n_step
        self.n_server = n_server
        self.step_mode = step_mode
        print("step_mode", self.step_mode)
        if self.step_mode == "AllToAll":
            self.total_count: int = n_step * n_client
        else:
            self.total_count: int = n_step * n_client / self.n_server
        print("total_count", self.total_count)
        self.received_step: int = 0
        self.it: int = 0

        # MPI attributes
        self.comm = comm
        self.comm_size = comm_size
        self.rank = rank

        # ADIOS2 setting
        self.thread_open = thread_open
        self.thread_data = thread_data
        self.adios = adios2.ADIOS()  # comm=self.comm  # removing the communicator according to discussion with adios devs https://github.com/ornladios/ADIOS2/issues/3721
        self.io_dict: Dict[int, adios2.adios2.IO] = {}
        self.engine_dict: Dict[int, adios2.adios2.Engine] = {}
        self.first_message: bool = True
        self.key_it: int = 0
        self.thread_comms = []
        self.time_steps = []
        self.generator = circular_integer_generator(list(range(self.n_server)))

        # Thread pool executor
        self.client_open_pool = ThreadPoolExecutor(max_workers=self.n_client)

        # Data attributes
        self.data_queue: List[npt.ArrayLike] = []
        self.data_lock: threading.RLock = threading.RLock()

    def start(self):
        """
        sets up the study
        """
        comm = MPI.COMM_WORLD
        thread_ranks = range(2)

        for thread_rank in thread_ranks:
            # Split the main communicator into separate communicators for each thread
            self.thread_comms.append(comm.Split(color=thread_rank, key=comm.Get_rank()))

        # Engine data extraction in a thread pool
        if self.thread_data:
            data_processing_thread = threading.Thread(target=self.get_data_thread)
            data_processing_thread.start()

            engine_open_thread = threading.Thread(target=self.init_engine_thread)
            engine_open_thread.start()

            receive_thread = threading.Thread(target=self.receive)
            receive_thread.start()

    def init_engine_thread(self):
        """
        while loop over init_io_engine
        """
        print(f"Start init engine on rank {self.rank} in a separate thread..")
        while True:
            for cid in range(self.n_client):
                if cid not in self.engine_dict or self.engine_dict[cid] is None:
                    self.init_io_engine(cid)
            # check if any engine is None
            if not any(engine is None for engine in self.engine_dict.values()):
                break

        print("all clients opened")

    def init_io_engine(self, cid: int):
        """
        create io and engine for client cid
        """
        if cid not in self.engine_dict:
            self.io_dict[cid] = self.adios.DeclareIO(f"readerIO-{cid}")
            self.io_dict[cid].SetEngine("SST")
            self.io_dict[cid].SetParameter("OpenTimeoutSecs", "1")
            self.engine_dict[cid] = None

        sst_filename = f"melissa.sid-{cid}"
        if self.rank == 0:
            # check if sst_filename exists with os
            # print(f"Rank {self.rank}: checking if {sst_filename} exists at {datetime.now()}")
            if os.path.exists(f"{sst_filename}.sst"):
                print(f"Rank {self.rank}: found {sst_filename}.sst")
                # bcast sst_filename to other ranks
                sst_filename = self.thread_comms[0].bcast(sst_filename, root=0)
                # use a barrier to ensure its opened at the same time
                # print(f"Rank {self.rank}: initializing engine {cid} at {datetime.now()}")
                # self.thread_comms[0].Barrier()
                self.engine_dict[cid] = self.io_dict[cid].Open(sst_filename, adios2.Mode.Read)
                # print(f"Rank {self.rank}: initialized engine {cid} at {datetime.now()}")
            else:
                self.thread_comms[0].bcast(None, root=0)
                return
        else:
            # wait for the bcast from rank 0
            sst_filename = self.thread_comms[0].bcast(None, root=0)
            if sst_filename is not None:
                try:
                    print(f"Rank {self.rank}: initializing engine {cid} at {datetime.now()}")
                    # self.thread_comms[0].Barrier()
                    self.engine_dict[cid] = self.io_dict[cid].Open(sst_filename, adios2.Mode.Read)
                    print(f"Rank {self.rank}: initialized engine {cid} at {datetime.now()}")
                except Exception as e:
                    print(f"Init io {cid} failed at {datetime.now()} "
                          f"on rank {self.rank} with {e}")
                    pass
            else:
                return

        print(
            f"Rank {self.rank}: [Connection] received connection message "
            f"from simulation {cid} at {datetime.now()}"
        )

    def get_data(self, cid):
        """
        extract data from engines
        """

        if not self.rank == 0:
            self.engine_dict[cid].BeginStep(mode=adios2.StepMode.Read)
            print(f"BeginStep - message: sim-id {cid} on rank {self.rank}")
        field_name = "array"
        data_id = self.io_dict[cid].InquireVariable(field_name)
        if data_id:
            # get metadata from first message only
            if self.first_message:
                self.global_size = data_id.Shape()[-1]
                data_type = data_id.Type()
                if self.step_mode == "AllToAll":
                    self.start_idx, end_idx = compute_load(
                        self.rank, self.global_size, self.comm_size
                    )
                    self.data_size = end_idx - self.start_idx + 1
                else:
                    self.data_size = self.global_size
                    self.start_idx = 0
                self.data = np.zeros(self.data_size, dtype=data_type)
                self.first_message = False
                print(f"1st message on rank {self.rank}: global_size {self.global_size}, "
                      f"local_size {self.data_size}, data_type {data_type}")
            # get data, time_step, simulation_id and message size
            data_id.SetSelection(([self.start_idx], [self.data_size]))
            self.engine_dict[cid].Get(data_id, self.data)
            time_step = self.engine_dict[cid].CurrentStep()
            simulation_id = cid
            self.engine_dict[cid].EndStep()
            print(f"EndStep - message: sim-id {simulation_id}, time-step {time_step}, "
                  f"data {self.data[:]} on rank {self.rank}")
            with self.data_lock:
                self.data_queue.append(self.data)
                self.time_steps.append((simulation_id, time_step))
            self.received_step += 1

    def get_data_thread(self):
        """
        while loop over get_data
        """
        print(f"Start getting data on rank {self.rank} in a separate thread..with n_clients {self.n_client}")
        while not self.all_steps_received():  # self.received_step < self.total_count:
            for cid in range(self.n_client):
                if self.rank == 0:
                    data_id = self.check_data(cid)
                    if data_id:
                        serv_proc = next(self.generator)
                        self.thread_comms[1].bcast((cid, serv_proc), root=0)
                        self.get_data(cid)
                    else:
                        self.thread_comms[1].bcast((None, None), root=0)
                        continue
                else:
                    cid, serv_proc = self.thread_comms[1].bcast(None, root=0)
                    if cid is not None:
                        self.get_data(cid)
                    else:
                        continue
        print("all clients received")

    def check_data(self, cid):
        if not self.engine_dict or cid not in self.engine_dict:
            return None

        if (
            self.engine_dict[cid]
            and self.engine_dict[cid].BeginStep(mode=adios2.StepMode.Read)
                != adios2.StepStatus.EndOfStream
        ):
            print(f"BeginStep - message: sim-id {cid} on rank {self.rank}")
            field_name = "array"
            data_id = self.io_dict[cid].InquireVariable(field_name)
        else:
            data_id = None

        return data_id

    def all_steps_received(self):
        done = self.received_step == self.total_count
        result = self.comm.allreduce(done, op=MPI.LAND)
        return result

    def receive(self):
        """
        receiving function
        """
        print(f"Start receiving on rank {self.rank}..")
        data: Optional[npt.ArrayLike] = None
        while self.received_step < self.total_count or self.data_queue:
            # check if self.data_queue is empty
            if self.data_queue:
                with self.data_lock:
                    data = self.data_queue.pop(0)
                self.compute_stats(data)

        # save self.time_steps to json file
        with open(f"time_steps_{self.rank}.json", "w") as f:
            json.dump(self.time_steps, f)
        print(f"Received all time steps on rank {self.rank}")

        # use mpi barrier here
        self.comm.Barrier()
        [engine.Close() for engine in self.engine_dict.values()]

    def compute_stats(self, data: npt.ArrayLike):
        """
        this function should compute iterative statistics with data
        """
        self.it += 1
        # print(f"Iterative statistic iteration {self.it} fake computation on rank {self.rank}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--n_client", type=int, default=1, help="number of clients"
    )
    parser.add_argument(
        "--client_np", type=int, default=1, help="number of proc per client"
    )
    parser.add_argument(
        "--n_step", type=int, default=1, help="number of produced time steps per client"
    )
    parser.add_argument(
        "--thread_open", action='store_true', help="engine open handled in thread pool"
    )
    parser.add_argument(
        "--thread_data", action='store_true', help="engine data extraction handled in thread"
    )
    parser.add_argument(
        "--step_mode", type=str, default="AllToAll", help="step mode: AllToAll or RoundRobin"
    )
    parser.add_argument(
        "--n_server", type=int, default=2, help="number of active server processes"
    )
    args = parser.parse_args()

    t0 = time.time()

    # MPI setting
    comm = MPI.COMM_WORLD
    comm_size = comm.Get_size()
    rank = comm.Get_rank()

    # Initialize Melissa server
    melissa_server = Server(
        args.n_client, args.client_np, args.n_step, args.thread_open, args.thread_data, step_mode=args.step_mode, n_server=args.n_server
    )

    # Launch study
    print(f"Start server on rank {rank}..")
    melissa_server.start()

    tf = time.time()
    print(f"Elapsed time: {tf-t0} seconds on rank {rank}")
