# Adios2 Melissa simple demo

This repository was built in the context of [this discussion](https://github.com/ornladios/ADIOS2/discussions/3675). It evaluates in a simple context if [adios2](https://adios2.readthedocs.io/en/latest/index.html) could replace the current [Melissa API](https://melissa.gitlabpages.inria.fr/melissa/#user-interface).

**Note**: although the reconstituted structure aims at reproducing Melissa standard workflow, it doesn't exactly reflect the real behavior nor the tasks performed by an actual [Melissa Server](https://melissa.gitlabpages.inria.fr/melissa/melissa-server/#melissa-server). Examples of discrepancies are given at the end of this document.

## Description
Similarly to the Melissa architecture, this repository comprises three core Python scripts:
- a launcher script which submits the server and clients OpenMPI jobs
- a parallel server script (similar to the [sensitivity analysis server](https://melissa.gitlabpages.inria.fr/melissa/melissa-server/#the-sensitivity-analysis-server) of Melissa) which connects to the clients and consumers their produced data
- a parallel client script which produces data and send them to the server

The data transfer is handled with adios2 [SST engines](https://adios2.readthedocs.io/en/latest/engines/engines.html#sst-sustainable-staging-transport) in an "AllToAll" step distribution mode and an **MxN** data distribution scheme.

## Requirements
The following dependencies must be installed for these scripts to be executable:
- Adios2
- An MPI implementation
- Python 3.8 or newer

**Note**: instructions on how to install adios2 are available on the [documentation](https://adios2.readthedocs.io/en/latest/setting_up/setting_up.html).

In addition, recent version of the following Python dependencies are needed:
- `adios2` (this comes with adios2 but it must be added to the `PYTHONPATH`)
- `mpi4py`
- `numpy`

## Usage
The study can be launched with the following command:
```py
python3 launcher.py --server_np=<nb_server_ranks> --n_client=<nb_clients> --client_np=<nb_client_ranks> --n_step=<nb_produced_steps>
```

It comes with other optional attributes to trigger different levels of Python multi-threading:
```py
python3 launcher.py -h
usage: launcher.py [-h] [--server_np SERVER_NP] [--n_client N_CLIENT] [--client_np CLIENT_NP] [--n_step N_STEP] [--thread_open] [--thread_data]

optional arguments:
  -h, --help            show this help message and exit
  --server_np SERVER_NP
                        number of server ranks
  --n_client N_CLIENT   number of clients
  --client_np CLIENT_NP
                        number of client ranks
  --n_step N_STEP       number of produced time steps per client
  --thread_open         engine open handled in thread pool
  --thread_data         engine data extraction handled in thread
```

Hence the following command will handle message reception in a separate thread:
```py
python3 launcher.py --server_np=<nb_server_ranks> --n_client=<nb_clients> --client_np=<nb_client_ranks> --n_step=<nb_produced_steps> --thread_data
```

While this one will try to open engines with a thread pool:
```py
python3 launcher.py --server_np=<nb_server_ranks> --n_client=<nb_clients> --client_np=<nb_client_ranks> --n_step=<nb_produced_steps> --thread_open
```

## Simplification wrt real Melissa conditions
This simplified example was assembled quickly by removing multiple levels of complexity associated with a real Melissa execution. For instance in real conditions:
1. the launcher sends messages to all server ranks about the client job statuses. Rank 0 of the server sends messages to the launcher (e.g. client submission and client restart requests as well as life signals).
2. the number of concurrent clients connected to the server can vary over time due to the cluster's occupancy or to the limited amount of available resources requested for the study (see the [scheduling strategies](https://melissa.gitlabpages.inria.fr/melissa/melissa-scheduler/#melissa-schedulers)). The server must hence be capable of opening new engines dynamically at any time during the study.
3. the compute stats function performs computations with the received data which are then discarded. For memory efficiency reasons, it is hence important to keep a certain balance between the data reception and the statistical data processing.