"""Adios2/Melissa dummy launcher"""
import argparse
import glob
import os
import subprocess
import sys
import time


def submit_clients(n_client: int, client_np: int, n_step, step_mode: str):
    """
    submits as many clients as requested
    """
    print("Start client submission..")
    submission_cmd = []
    process_list = []
    for cid in range(n_client):
        submission_cmd = [
            "mpirun",
            "-np",
            f"{client_np}",
            "-x",
            f"SIMU_ID={cid}",
            "python3",
            "client.py",
            f"--n_step={n_step}",
            f"--step_mode={args.step_mode}"
        ]
        with open(f"client-{cid}.out", "w") as stdout:
            with open(f"client-{cid}.err", "w") as stderr:
                process = subprocess.Popen(
                    args=submission_cmd,
                    env=os.environ,
                    stdin=subprocess.DEVNULL,
                    stdout=stdout,
                    stderr=stderr,
                    universal_newlines=True
                )
                process_list.append(process)
                print(f"submitted client {cid + 1}/{n_client} "
                      f"with command {submission_cmd}")
    return process_list


def submit_server(
        server_np: int, n_client: int, n_step: int, thread_open: bool, thread_data: bool
):
    """
    submits server
    """
    print("Server submission..")
    submission_cmd = [
        "mpirun",
        "--output-filename", "server",
        "-np",
        f"{server_np}",
        "python3",
        "server.py",
        f"--n_client={n_client}",
        f"--n_step={n_step}",
        f"--step_mode={args.step_mode}",
        f"--n_server={args.server_np}"
    ]
    if thread_open:
        submission_cmd.append("--thread_open")
    if thread_data:
        submission_cmd.append("--thread_data")

    process = subprocess.Popen(
        args=submission_cmd,
        env=os.environ,
        stdin=subprocess.DEVNULL,
        universal_newlines=True
    )
    print(f"submitted server with command {submission_cmd}")
    return process


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--server_np", type=int, default=1, help="number of server ranks"
    )
    parser.add_argument(
        "--n_client", type=int, default=1, help="number of clients"
    )
    parser.add_argument(
        "--client_np", type=int, default=1, help="number of client ranks"
    )
    parser.add_argument(
        "--n_step", type=int, default=1, help="number of produced time steps per client"
    )
    parser.add_argument(
        "--thread_open", action='store_true', help="engine open handled in thread pool"
    )
    parser.add_argument(
        "--thread_data", action='store_true', help="engine data extraction handled in thread"
    )
    parser.add_argument(
        "--step_mode", type=str, default="RoundRobin", help="step distribution mode"
    )
    args = parser.parse_args()

    t0 = time.time()

    # Clean any remaining files
    cwd = os.getcwd()
    rm_f = []
    print(f"Remove *.out *.err and *.sst files in cwd: {cwd}")
    for f in ["*.out", "*.err", "*.sst"]:
        rm_f.extend(glob.glob(f))
    for f in rm_f:
        os.remove(cwd + "/" + f)

    # Submit server
    server_process = submit_server(
        args.server_np, args.n_client, args.n_step, args.thread_open, args.thread_data
    )

    # to trigger an exception at engine opening
    print("2 seconds nap..")
    time.sleep(2)

    # Submit clients
    client_process = submit_clients(args.n_client, args.client_np, args.n_step, args.step_mode)

    # While loop
    server_rcode = 1
    while time.time() - t0 < args.n_client * 2 + 30:
        server_rcode = server_process.poll()
        if server_rcode == 0:
            print("server ran successfully1")
            sys.exit(0)

    # Kill unfinished subprocesses
    for id, proc in enumerate(client_process):
        client_rcode = proc.poll()
        if client_rcode == 0:
            print(f"client {id} ran successfully")
        else:
            print(f"client {id} killed with returncode {client_rcode}")
        proc.kill()

    server_rcode = server_process.poll()
    if server_rcode == 0:
        print("server ran successfully2")
    else:
        print(f"server killed with returncode {server_rcode}")
        server_process.kill()
